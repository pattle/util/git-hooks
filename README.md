# Useful git hooks

  These are useful hooks for git to check
  for formatting and analysis issues before you commit.

## Usage

   1. Clone the repo and enter the directory:

      `git clone https://git.pattle.im/pattle/util/git-hooks.git && cd git-hooks`

   2. Make a symbolic link for every hook to the git repo you want to add the hook to:

      `ln -s $(pwd)/pre-commit <pattle-location>/.git/hooks`

   3. Repeat step 2 for the
      [Matrix Dart SDK](https://git.pattle.im/pattle/library/matrix-dart-sdk)
      and the
      [Sqflite Store](https://git.pattle.im/pattle/library/matrix-dart-sdk-sqflite)
      projects.


   Because it's a symoblic link, when you pull this repo your hooks will be
   updated automatically.
